import { stripControlChars, stripHTML } from "./helpers"

export const parseFields = (element) => {
  const fieldRegex = /\[\[?(.+?)\]\]?/g
  const fields = Array.from(element.innerHTML.matchAll(fieldRegex))
  return fields.map((res) => {
    const rawMatch = res[0]
    const key = stripControlChars(stripHTML(rawMatch)).trim()
    return {
      key,
      rawMatch,
      index: res.index,
      element
    }
  })
}

export const hasCompleteField = (element) => {
  const text = element && element.innerHTML
  if (!text) return false
  const fieldStart = text.indexOf('[')
  if (fieldStart < 0) return false
  const fieldEnd = text.indexOf(']', fieldStart)
  if (fieldEnd < 0) return false

  return true
}

export const insertValue = (key, value) => {
  //console.log('insert value to nodes', document.querySelectorAll(`.key-${key}`))
  for (const e of document.getElementsByClassName(`key-${key}`)) {
    e.textContent = value
  }
}

/* Wrap entire field ("[field text]") in a single node and preserve overlapping styles
 * 
 * Exported HTML from google docs contains styles. Styles are represented as span with classes.
 * Fields might overlap with such a span. mergeFieldStyle's job is to identify
 * all overlapping `span`s, cut them off, and re-wrap field text with them.
 * e.g. <span class="red">Not field text [field</span> text] would become:
 * <span class="red">Not field text </span><span class="red">[field text]</span>
 *
 * Fields wrapped in a single node can then be easily substituted by field.innerText = 'value'
 *
 * Yes. I know. It is messy. Roast me.
 */
export const isolateField = (field) => {
  const { key, rawMatch, index, element: e } = field
  // console.log('want to insert ', value, ' to ', e.innerHTML)

  const bracketStart = index, bracketEnd = index + rawMatch.length
  //console.log('bracket start index: ', bracketStart)
  //console.log('bracket end index: ', bracketEnd)

  const nodes = []
  const startTagStack = []
  // performance.mark('isolateField:match:start')
  for (const match of e.innerHTML.matchAll(/<(\/?)(\w+)(.*?)>/g)) {
    //console.log('match: ', match)
    const summary = {
      tag: match[0],
      isEnd: match[1] === '/',
      tagName: match[2],
      index: match.index,
    }
    //console.log('summary', summary)
    if (!summary.isEnd) {
      startTagStack.push(summary)
    } else {
      const lastStart = startTagStack.pop()
      if (!lastStart || lastStart.tagName !== summary.tagName) {
        throw new EvalError('unexpected: couldn\'t find any start tag for', summary.tag)
      }
      summary.startTagMatch = lastStart
      nodes.push(summary)
    }
  }
  // performance.mark('isolateField:match:end')
  // console.log('measure: ', performance.measure('isolateField:match', 'isolateField:match:start', 'isolateField:match:end'))
  // console.log('measures: ', performance.getEntries())
  if (startTagStack.length !== 0) {
    throw new EvalError('unexpected dangling start tags')
  }
  //console.log('nodes: ', nodes)
  const isInBracket = (m) => m.index > bracketStart && m.index < bracketEnd
  const inBracketMatches = nodes.filter(m => isInBracket(m) || isInBracket(m.startTagMatch))
  //console.log('inBracketMatches', inBracketMatches)
  // construct replacement string
  let preEndTagsStr = ''
  let startTagsStr = ''
  let endTagsStr = ''
  let postStartTagsStr = ''
  for (const match of inBracketMatches) {
    //console.log('in bracket match', match)
    startTagsStr += startTagsStr + match.startTagMatch.tag

    if (!isInBracket(match)) {
      postStartTagsStr += match.startTagMatch.tag
      if (!isInBracket(match.startTagMatch)) {
        throw new EvalError('unexpected pair of tags that are both outside brackets')
      } else {
        endTagsStr = match.tag + endTagsStr
      }
    } else {
      startTagsStr = match.startTagMatch.tag + startTagsStr
      if (!isInBracket(match.startTagMatch)) {
        preEndTagsStr += match.tag
      } else {
        endTagsStr = match.tag + endTagsStr
      }
    }
  }
  const replacement = `${preEndTagsStr}${startTagsStr}<span class='key-${key}'>[${key}]</span>${endTagsStr}${postStartTagsStr}`
  //console.log('original innerHTML', e.innerHTML)
  //console.log('to replace', rawMatch)
  //console.log('replacement: ', replacement)
  e.innerHTML = e.innerHTML.replace(rawMatch, replacement)
}

const fieldMapper = (acc, field) => {
  acc[field.key] = acc[field.key] ? [...acc[field.key], field] : [field]
  return acc
}

export const findBracketedTextNodes = (document) => {
  return (function* textNodeIterator(){
    const result = document.evaluate('//text()[contains(., "[")]', document, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE)

    let node
    while ((node = result.iterateNext())) {
      yield node
    } 
  })()
}

/* Maps fields to corresponding nodes.
 * Substituting a field = writing innerText = 'value' to each node
 *
 * @param {Array} An array of text nodes which innerText contains a starting square bracket ("[")
 *
 * @return {Object} An object with field name as keys, array of nodes as values
 */
export const mapFields = (textNodes) => {
  let mappedFields = {}
  for (const textNode of textNodes) {
    const container = textNode.parentElement
    // let text =  container.innerHTML
    let fields
    if (hasCompleteField(container)) {
      fields = parseFields(container)
    } else if (hasCompleteField(container.parentElement)) {
      fields = parseFields(container.parentElement)
    } else {
      //console.log('no complete field for ', container)
      continue
    }
    mappedFields = fields.reduce(fieldMapper, mappedFields)
  }
  return mappedFields
}

/*
Corner case?

<span class="b"><span class="a">[field</span></span> text]

-> This never happens in google doc. styled texts are always wrapped in ONE single styled node.
*/