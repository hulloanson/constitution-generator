export const stripControlChars = (fieldStr) => fieldStr.replaceAll(/(^\[)|(\]$)/g, '')

export const stripHTML = (str) => {
  const doc = new DOMParser().parseFromString(`<html><body>${str}</body></html>`, 'text/html')
  return doc.body.textContent || doc.body.innerText || ""
}
