import { findBracketedTextNodes, isolateField, insertValue, mapFields } from "./fields";

const update = (updateState) => {
  for (const key in updateState) {
    const value = updateState[key] || `[${key}]`
    insertValue(key, value)
  }
}


const main = () => {
  const textNodes = findBracketedTextNodes(document)
  const mappedFields = mapFields(textNodes)
  for (const key in mappedFields) {
    // console.log('key: ', key, ';field: ', mappedFields[key])
    mappedFields[key].forEach(field => isolateField(field))
  }
  //console.log('final mappedFields:', mappedFields)
  //console.log('posting to parent...')
  window.parent.postMessage({ type: 'fields', data: Object.keys(mappedFields) })

  window.addEventListener('message', e => {
    // console.log('received update from parent: ', e.data)
    update(e.data)
  })
}

main()