import debounce from 'lodash/debounce'

let iframe = null
let state = {}

const fetchTemplate = async(name) => {
  const res = await fetch(`${window.location.origin}/templates/${name}.html`)
  if (!res.ok) throw res
  return res.text()
}
const makeIframe = (templateHTML) => {
  iframe = document.createElement('iframe')
  iframe.srcdoc = templateHTML
  iframe.addEventListener('load', () => {
    const ifDoc = iframe.contentWindow.document
    const scriptTag = ifDoc.createElement('script')
    scriptTag.src = '/iframe.js'
    ifDoc.body.appendChild(scriptTag)
  })
  return iframe
}

// window is a Window object
const sendState = (window, state) => {
  window.postMessage(state)
}

const updateState = debounce((field, value) => {
  const update = { [field]: value }
  state = { ...state, ...update }
  sendState(iframe.contentWindow, update)
}, 100)

const constructField = (fieldName, value = '') => {
  const label = document.createElement('label')
  const id = `input-${fieldName}`
  label.htmlFor = id
  label.innerText = fieldName
  const input = document.createElement('input')
  input.value = value
  input.id = id
  input.addEventListener('input', (e) => updateState(fieldName, e.target.value))
  const container = document.createElement('div')
  container.appendChild(label)
  container.appendChild(input)
  return container
}

const constructForm = (fields) => {
  // console.log('fields: ', fields)
  const container = document.createElement('div')
  fields.forEach(f => {
    container.appendChild(constructField(f))
  })
  return container
}

window.addEventListener('message', e => {
  // console.log('e.data', e.data)
  if (e.data.type !== 'fields') return
  document.getElementById('fields').appendChild(constructForm(e.data.data))
})
document.addEventListener('DOMContentLoaded', async function onload() {
  const template = 'org-constitution'
  const templateHTML = await fetchTemplate(template)
  document.body.appendChild(makeIframe(templateHTML))
  document.getElementById('copy-button').addEventListener('click', e => {
    if (iframe === null) return
    const sel = iframe.contentWindow.getSelection()
    sel.selectAllChildren(iframe.contentWindow.document.body)
    iframe.contentWindow.document.execCommand('copy')
    sel.empty()
    alert('Copy 好！依家你可以開 Google Doc / Word 貼上份章程啦 :)')
  })
})
