const DotenvWebpackPlugin = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Dotenv = require('dotenv-webpack')

const dev = process.env.NODE_ENV !== 'production'

module.exports = {
  entry: {
    main: './src/js/main/index.js',
    iframe: './src/js/iframe/index.js'
  },
  output: {
    filename: '[name].js',
    path: __dirname + '/public'
  },
  resolve: {
    extensions: [".js"],
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          dev ? 'style-loader' : MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "sass-loader",
            // options: {
              // includePaths: ["./node_modules/bootstrap/scss"]
            // }
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        }
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.(png)|(jpg)$/,
        loader: 'url-loader'
      },
      {
        test: /index\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {

            }
          },
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/html/index.html',
      excludeChunks: ['iframe']
    }),
    new MiniCssExtractPlugin(),
    new Dotenv({
      safe: true, // load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
      allowEmptyValues: false, // allow empty variables (e.g. `FOO=`) (treat it as empty string, rather than missing)
      systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
      silent: false, // hide any errors
      defaults: false, // load '.env.defaults' as the default values if empty.
    }),
  ]
};
