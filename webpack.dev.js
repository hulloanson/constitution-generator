const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const webpack = require('webpack');
const getPort = require('get-port')
const dotenv = require('dotenv')
dotenv.config()

function* preferredPorts() {
  let base = 8000

  while (base < 65535) {
    yield base++
  }
}

module.exports = async () => {
  return merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
      contentBase: path.resolve(__dirname, 'public'),
      port: await getPort({ port: preferredPorts() }),
      hot: true,
      disableHostCheck: true, // So it can be proxied
      host: '0.0.0.0',
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
    ],
  })
};
